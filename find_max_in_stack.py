'''
Стек — абстрактная структура данных, поддерживающая операции push и pop.
Несложно реализовать стек так, чтобы обе эти операции работали за константное время. 
В данной задача ваша цель — расшить интерфейс стека так, чтобы он дополнительно поддерживал операцию max
и при этом чтобы время работы всех операций по-прежнему было константным.

Формат ввода. 
Первая строка содержит число запросов q. Каждая из последующих q строк задает запрос в одном из следующих
форматов: push v, pop, или max.

Формат вывода. 
Для каждого запроса max выведите (в отдельной строке) текущий максимум на стеке. 

Sample Input 1:
5
push 2
push 1
max
pop
max
Sample Output 1:
2
2

Sample Input 2:
5
push 1
push 2
max
pop
max
Sample Output 2:
2
1

Sample Input 3:
10
push 2
push 3
push 9
push 7
push 2
max
max
max
pop
max 
Sample Output 3:
9
9
9
9
'''
import sys

count = int(sys.stdin.readline().strip())
stack = []
stack_max = []
for i in range(count):
    data = [str(i) for i in sys.stdin.readline().strip().split()]
    if data[0] == 'push':
        stack.append(int(data[1]))
        if stack_max:
            stack_max.append(stack[-1] if stack[-1] > stack_max[-1] else stack_max[-1])
        else:
            stack_max.append(stack[-1])
    if data[0] == 'pop':
        last_elem = stack.pop()
        if stack_max:
            if len(stack_max) == 1 or not stack:
                stack_max = [0]
            else:
                stack_max.pop()
    if data[0] == 'max':
        sys.stdout.write(str(stack_max[-1]) + "\n")