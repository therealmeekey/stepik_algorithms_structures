'''
Найти максимум в каждом окне размера m данного массива чисел A[1..n].
Наивный способ решить данную задачу — честно просканировать каждое окно и найти в нем максимум. Время работы такого 
алгоритма — O(nm). Ваша задача — реализовать алгоритм со временем работы O(n).

Формат ввода. 
Первая строка входа содержит число n, вторая — массив A[1..n], третья — число m.

Формат вывода. 
n - m + 1 максимумов, разделенных пробелами.

Sample Input 1:
3
2 1 5
1
Sample Output 1:
2 1 5

Sample Input 2:
8
2 7 3 1 5 2 6 2
4
Sample Output 2:
7 7 5 6 6
'''
import sys

n = int(sys.stdin.readline().strip())
data = [int(i) for i in sys.stdin.readline().strip().split()]
m = int(sys.stdin.readline().strip())
stack_in, stack_out = [], []

out_stack = []

for i in data[:m]:
    stack_in.append((i, max(i, stack_in[-1][1]) if stack_in else i))
out_stack.append(str(stack_in[-1][1]))

for i in data[m:]:
    if not stack_out:
        while stack_in:
            value = stack_in.pop()[0]
            stack_out.append((value, max(value, stack_out[-1][1]) if stack_out else value))
    stack_out.pop()
    stack_in.append((i, max(i, stack_in[-1][1]) if stack_in else i))
    if not stack_in:
        out_stack.append(str(stack_out[-1][1]))
    elif not stack_out:
        out_stack.append(str(stack_in[-1][1]))
    else:
        out_stack.append(str(max(stack_in[-1][1], stack_out[-1][1])))
sys.stdout.write(' '.join(out_stack) + "\n")